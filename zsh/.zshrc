# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
# fino blinks duellj fox bira robbyrusell
ZSH_THEME="jreese"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git colorize zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
#export PATH=/home/fernando/bin:/usr/lib/lightdm/lightdm:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/share/iron
path=(  /home/oramirez/bin
        /usr/local/bin
        /usr/bin
        /bin
        /usr/games
     )
# * + * + * + * + * + 
# 
#       MINE!!!!
#
# * + * + * + * + * + 
setopt  AutoParamKeys       # Default
setopt  AutoParamSlash      # Default
setopt  AutoRemoveSlash     # Default
setopt  GlobComplete
setopt  ListPacked
setopt  ListTypes           # Default
setopt  BadPattern
setopt  CaseGlob            # Default
setopt  CaseMatch           # Default
setopt  NumericGlobSort     
setopt  AppendHistory       # Default
setopt  ExtendedHistory     
setopt  ExtendedGlob
setopt  HistExpireDupsFirst
setopt  HistFindNoDups
setopt  HistIgnoreDups
setopt  HistIgnoreSpace
setopt  HistReduceBlanks
setopt  HistSaveByCopy
setopt  HistSaveNoDups
setopt  HistVerify
setopt  MultIOs            # multiple redirection
setopt  Aliases
setopt  Clobber             # >/>> allowed to truncate/create
setopt  RMStarWait          # Lololol
setopt  ShortLoops
setopt  BGNice              # BG jobs => lower priority
setopt  CBases
setopt  ZLE

export HISTFILE=$HOME/.zsh_history
export HISTSIZE=100100
export SAVEHIST=100000
export LSCOLORS=gxfxcxdxbxegedabagacad


fpath=($HOME/Documents/zshfunctions $fpath)
autoload -U aman
autoload -U zargs
autoload -U zmv

zmodload zsh/mapfile

zle_highlight+=(isearch:fg=blue,bold)

bindkey '^R' history-incremental-pattern-search-backward
bindkey '^S' history-incremental-pattern-search-forward

alias open="gvfs-open"

export ZSH
