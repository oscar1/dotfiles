# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
# fino blinks duellj fox bira robbyrusell
ZSH_THEME="jreese"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"
REPORTTIME=5

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
path=( /home/oramirez/bin
       /home/oramirez/.cabal/bin
       /usr/local/sbin
       /usr/local/bin
       /usr/sbin
       /usr/bin
       /sbin
       /bin
       /usr/games
       /usr/local/games
     )

# * + * + * + * + * + 
# 
#       MINE!!!!
#
# * + * + * + * + * + 
setopt  AutoParamKeys       # Default
setopt  AutoParamSlash      # Default
setopt  AutoRemoveSlash     # Default
setopt  GlobComplete
setopt  ListPacked
setopt  ListTypes           # Default
setopt  BadPattern
setopt  CaseGlob            # Default
setopt  CaseMatch           # Default
setopt  NumericGlobSort     
setopt  AppendHistory       # Default
setopt  ExtendedHistory     
setopt  ExtendedGlob
setopt  HistExpireDupsFirst
setopt  HistFindNoDups
setopt  HistIgnoreDups
setopt  HistIgnoreSpace
setopt  HistReduceBlanks
setopt  HistSaveByCopy
setopt  HistSaveNoDups
setopt  HistVerify
setopt  Aliases
setopt  Clobber             # >/>> allowed to truncate/create
setopt  ShortLoops
setopt  BGNice              # BG jobs => lower priority
setopt  CBases
setopt  ZLE

export dcs=$HOME/'Documents'

export HISTFILE=$HOME/.zsh_history
export HISTSIZE=100100
export SAVEHIST=100000
export LSCOLORS=gxfxcxdxbxegedabagacad


fpath=($HOME/scripts/zshfunctions $fpath)
autoload -U aman
autoload -U opn
autoload -U zargs
autoload -U zmv

zle_highlight+=(isearch:fg=blue,bold)

bindkey '^R' history-incremental-pattern-search-backward
bindkey '^S' history-incremental-pattern-search-forward

bindkey '^P'  history-beginning-search-backward
bindkey '^N'  history-beginning-search-forward

export ZSH

alias tree='tree -CF'
export ThreeDTOUCH_BASE="/usr/share/3DTouch"
export EDITOR=/usr/bin/vim

#source /opt/ros/fuerte/setup.zsh
#export ROS_PACKAGE_PATH=~/ros:/opt/ros/fuerte/share:/opt/ros/fuerte/stacks
#export ROS_WORKSPACE=~/ros
#export PYTHONPATH=$PYTHONPATH:/home/oramirez/ros/tracker_camshift/src/

##catkin_ws
#source /opt/ros/groovy/setup.zsh
#export ROS_PACKAGE_PATH=/opt/ros/groovy/share:/opt/ros/groovy/stacks:/home/oramirez/catkin_ws
#source /home/oramirez/catkin_ws/devel/setup.zsh
##export PYTHONPATH=$PYTHONPATH:/home/oramirez/catkin_ws/devel/lib/python2.7/dist-packages

#Kinova
source /opt/ros/groovy/setup.zsh
export ROS_PACKAGE_PATH=/opt/ros/groovy/share:/opt/ros/groovy/stacks:/home/oramirez/kinova
export KINOVA_WORKSPACE=/home/oramirez/kinova
source /home/oramirez/kinova/devel/setup.zsh
