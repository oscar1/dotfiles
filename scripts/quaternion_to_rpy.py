# http://www.chrobotics.com/library/understanding-quaternions
import math

#a = w
#b = x
#c = y
#d = z

def get_roll(a, b, c, d):
    num = 2 * ((a*b) + (c*d))
    den = a**2 - b**2 - c**2 + d**2
    return math.atan2(num, den)

def get_pitch(a, b, c, d):
    r = 2 * ((b*d) - (a*c))
    if abs((abs(r) - 1)) < 0.0000001:
        r = 1 * (r / abs(r))
    return -1 * math.asin(r)

def get_yaw(a, b, c, d):
    num = 2 * ((a*d) + (b*c))
    den = a**2 + b**2 - c**2 - d**2
    return math.atan2(num, den)

def convert(a, b, c, d):
    r = get_roll(a, b, c, d)
    p = get_pitch(a, b, c, d)
    y = get_yaw(a, b, c, d)

    print "Roll: %f" % r
    print "Pitch: %f" % p
    print "yaw: %f" % y

    print "Roll: %f" % math.degrees(r)
    print "Pitch: %f" % math.degrees(p)
    print "yaw: %f" % math.degrees(y)
